Follow instruction to set up the app locally:

1. `git clone git@bitbucket.org:elan13/bee-game.git`

1. `cd ./bee-game/`

1. `composer install`

1. Clone `.env` to `.env.local` and **add your own env variables**

1. `php bin/console doctrine:migrations:migrate`

1. `php bin/console doctrine:fixtures:load`

1. run `php bin/console server:run` 
and  the app is accessible http://127.0.0.1:8000 