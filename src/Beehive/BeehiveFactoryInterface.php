<?php

namespace App\Beehive;


interface BeehiveFactoryInterface
{
    /**
     * @param array $beehiveConfig
     * @return array
     */
    public function create(array $beehiveConfig = []): array;
}