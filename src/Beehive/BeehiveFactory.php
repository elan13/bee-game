<?php

namespace App\Beehive;


use App\Controller\BeePlayController;
use App\Entity\Bee;
use App\Entity\BeeType;
use App\Repository\BeeTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class BeehiveFactory implements BeehiveFactoryInterface
{
    private $em;
    private $session;
    private $beeTypeRepository;


    /**
     * BeehiveFactory constructor.
     * @param EntityManagerInterface $em
     * @param SessionInterface $session
     * @param BeeTypeRepository $beeTypeRepository
     */
    public function __construct(EntityManagerInterface $em, SessionInterface $session, BeeTypeRepository $beeTypeRepository)
    {
        $this->em = $em;
        $this->session = $session;
        $this->beeTypeRepository = $beeTypeRepository;
    }


    /**
     * @param array $beehiveConfig [[type=>'value', quantity=>'value'],]
     * @return array
     * @throws \Exception
     */
    public function create(array $beehiveConfig = []): array
    {
        $types = $this->beeTypeRepository->findAll();
        $typeNames = array_map(function($bee){ return $bee->getName(); }, $types);

        $beehive = [];
        foreach ($beehiveConfig as $beehiveConfig) {
            if (!isset($beehiveConfig['type'], $beehiveConfig['quantity']) || !is_numeric($beehiveConfig['quantity'])) {
                throw new \Exception('Invalid configuration. Config should contain type and quantity(int)');
            }

            if (false === ($typeKey = array_search($beehiveConfig['type'], $typeNames))) {
                throw new \Exception(sprintf('Invalid %s', BeeType::class));
            }

            $beehive = array_merge($beehive, $this->createBeesByType($types[$typeKey], $beehiveConfig['quantity']));
        }

        return $beehive;
    }

    /**
     * @param BeeType $beeType
     * @param int $quantity
     * @return array
     */
    protected function createBeesByType(BeeType $beeType, int $quantity): array
    {
        $beeCell = [];

        while ($quantity-- > 0) {
            $bee = new Bee();
            $bee->setLeftLifepoints($beeType->getLifepoints());
            $bee->setSessionId($this->session->getId());
            $bee->setType($beeType);

            $beeCell[] = $bee;

            $this->em->persist($bee);
        }

        $this->em->flush();

        return $beeCell;
    }
}