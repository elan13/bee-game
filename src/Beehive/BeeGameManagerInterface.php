<?php

namespace App\Beehive;


interface BeeGameManagerInterface
{
    /**
     * start game; set or create bees
     *
     * @param array $beehive
     */
    public function start(array $beehive);

    /**
     * check if the game is in progress; as example you can set the time of start in session
     *
     * @return bool
     */
    public function isRunning(): bool;

    /**
     *  get bees from the current game
     *
     * @return array
     */
    public function getBeehive(): array;


    /**
     * as example deduct lifepoints
     *
     * @return bool
     */
    public function hitRandomBee(): bool;


    /**
     * stop game; as example you can close connection, remove data, etc.
     */
    public function stop();

}