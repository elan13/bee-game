<?php

namespace App\Beehive;


use App\Entity\Bee;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class BeeGameManager implements BeeGameManagerInterface
{
    const DEFAULT_BEEHIVE = [
        ['type' => 'queen', 'quantity' => 1],
        ['type' => 'worker', 'quantity' => 5],
        ['type' => 'drone', 'quantity' => 8],
    ];

    private $beehive;
    private $factory;

    private $em;
    private $session;


    /**
     * BeeGameManager constructor.
     * @param SessionInterface $session
     * @param EntityManagerInterface $em
     * @param BeehiveFactoryInterface $factory
     */
    public function __construct(SessionInterface $session, EntityManagerInterface $em, BeehiveFactoryInterface $factory)
    {
        $this->em = $em;
        $this->session = $session;

        $this->factory = $factory;

        $this->session->start();
    }

    public function __destruct()
    {
        $this->em->flush();
    }


    /**
     * create new beehive
     *
     * @param array $beehive  [[type=>'value', quantity=>'value'],]
     */
    public function start(array $beehive = [])
    {
        $this->session->set('bee_game_started', time());

        // remove bees if they exist
        foreach ($this->getBeehive() as $bee) {
            $this->removeBee($bee);
        }

        $this->beehive = $this->factory->create($beehive);
    }

    /**
     * @return bool
     */
    public function isRunning(): bool
    {
        return $this->session->has('bee_game_started');
    }

    /**
     * @return Bee[]
     */
    public function getBeehive(): array
    {
        if (empty($this->beehive)) {
            $this->beehive = $this->em
                ->getRepository(Bee::class)
                ->findBySession($this->session->getId())
            ;
        }

        return $this->beehive;
    }


    /**
     * by hitting, deduct lifepoints and if no points left Bee dies
     *
     * If Queen dies, every other bees die too
     *
     * @return bool
     */
    public function hitRandomBee(): bool
    {
        // if no bee to hit return false
        if (empty($this->beehive) && empty($this->getBeehive())) {
            $this->stop();
            return false;
        }

        return $this->hitBee($this->getRandomBee());
    }

    /**
     * @param Bee $bee
     * @return bool
     */
    protected function hitBee(Bee $bee)
    {
        $leftLifepoints = $this->deductLifepoints($bee);

        // if bee doesn't have lifepoints, delete it
        if ($leftLifepoints <= 0) {

            // if queen dies, every other bees die too
            if ($bee->isQueen()) {
                foreach ($this->beehive as $beeInHive) {
                    $this->removeBee($beeInHive);
                }
            } else {
                $this->removeBee($bee);
            }
        }

        return true;
    }

    /**
     * get random from Bee[]
     *
     * @return Bee|null
     */
    protected function getRandomBee()
    {
        return !empty($this->beehive) ? $this->beehive[array_rand($this->beehive)] : null;
    }

    /**
     * @param Bee $bee
     *
     * @return int|null  bee's leftLifepoints
     */
    protected function deductLifepoints(Bee $bee)
    {
        $leftLifepoints = $bee->getLeftLifepoints() - $bee->getType()->getMinDeductedLifepoints();
        $bee->setLeftLifepoints($leftLifepoints);

        return $bee->getLeftLifepoints();
    }


    /**
     * remove the Bee from database and from beehive
     *
     * @param Bee $bee
     */
    protected function removeBee(Bee $bee)
    {
        $key = array_search($bee, $this->beehive);

        if (false !== $key) {
            $this->em->remove($bee);

            unset($this->beehive[$key]);
        }
    }

    public function stop()
    {
        $this->session->remove('bee_game_started');
    }
}