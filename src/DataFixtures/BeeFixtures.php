<?php

namespace App\DataFixtures;

use App\Entity\BeeType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BeeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $queenType = new BeeType();
        $queenType->setName(BeeType::TYPE_QUEEN);
        $queenType->setLifepoints(100);
        $queenType->setMinDeductedLifepoints(8);

        $manager->persist($queenType);


        $workerType = new BeeType();
        $workerType->setName('worker');
        $workerType->setLifepoints(75);
        $workerType->setMinDeductedLifepoints(10);

        $manager->persist($workerType);


        $droneType = new BeeType();
        $droneType->setName('drone');
        $droneType->setLifepoints(50);
        $droneType->setMinDeductedLifepoints(12);

        $manager->persist($droneType);

        $manager->flush();
    }
}
