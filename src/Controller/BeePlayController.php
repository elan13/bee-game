<?php

namespace App\Controller;


use App\Beehive\BeeGameManagerInterface;
use App\Beehive\BeeGameManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BeePlayController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function index(Request $request, BeeGameManagerInterface $beeGameManager)
    {
        $beehive = [];

        // request to start game
        if ($request->request->has('start')) {
            $beeGameManager->start(BeeGameManager::DEFAULT_BEEHIVE);
        }

        // request to  hit some random bee, can return false if there no bee left
        if ($request->request->has('hit')) {
            $beeGameManager->hitRandomBee();
        }

        // if game started, get beehive instead of created new one
        if ($beeGameManager->isRunning()) {
            $beehive = $beeGameManager->getBeehive();
        }

        return $this->render('bee_game.html.twig', ['beehive' => $beehive]);
    }


}