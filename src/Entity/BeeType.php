<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BeeTypeRepository")
 */
class BeeType
{
    const TYPE_QUEEN = 'queen';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $lifepoints;

    /**
     * @ORM\Column(type="integer")
     */
    private $minDeductedLifepoints;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bee", mappedBy="type")
     */
    private $bees;

    public function __construct()
    {
        $this->bees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLifepoints(): ?int
    {
        return $this->lifepoints;
    }

    public function setLifepoints(int $lifepoints): self
    {
        $this->lifepoints = $lifepoints;

        return $this;
    }

    public function getMinDeductedLifepoints(): ?int
    {
        return $this->minDeductedLifepoints;
    }

    public function setMinDeductedLifepoints(int $minDeductedLifepoints): self
    {
        $this->minDeductedLifepoints = $minDeductedLifepoints;

        return $this;
    }

    /**
     * @return Collection|Bee[]
     */
    public function getBees(): Collection
    {
        return $this->bees;
    }

    public function addBee(Bee $bee): self
    {
        if (!$this->bees->contains($bee)) {
            $this->bees[] = $bee;
            $bee->setType($this);
        }

        return $this;
    }

    public function removeBee(Bee $bee): self
    {
        if ($this->bees->contains($bee)) {
            $this->bees->removeElement($bee);
            // set the owning side to null (unless already changed)
            if ($bee->getType() === $this) {
                $bee->setType(null);
            }
        }

        return $this;
    }
}
