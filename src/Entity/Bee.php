<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BeeRepository")
 */
class Bee
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $sessionId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BeeType", inversedBy="bees")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $leftLifepoints;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSessionId(): ?int
    {
        return $this->sessionId;
    }

    public function setSessionId(string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    public function getType(): ?BeeType
    {
        return $this->type;
    }

    public function setType(?BeeType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLeftLifepoints(): ?int
    {
        return $this->leftLifepoints;
    }

    public function setLeftLifepoints(int $leftLifepoints): self
    {
        $this->leftLifepoints = $leftLifepoints;

        return $this;
    }

    public function isQueen(): bool
    {
        return BeeType::TYPE_QUEEN == $this->getType()->getName();
    }
}
