<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190307220027 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bee (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, session_id VARCHAR(255) NOT NULL, left_lifepoints INT NOT NULL, INDEX IDX_9140CC69C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bee_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, lifepoints INT NOT NULL, min_deducted_lifepoints INT NOT NULL, UNIQUE INDEX UNIQ_6182552B5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bee ADD CONSTRAINT FK_9140CC69C54C8C93 FOREIGN KEY (type_id) REFERENCES bee_type (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bee DROP FOREIGN KEY FK_9140CC69C54C8C93');
        $this->addSql('DROP TABLE bee');
        $this->addSql('DROP TABLE bee_type');
    }
}
