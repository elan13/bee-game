<?php

namespace App\Repository;

use App\Entity\BeeType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BeeType|null find($id, $lockMode = null, $lockVersion = null)
 * @method BeeType|null findOneBy(array $criteria, array $orderBy = null)
 * @method BeeType[]    findAll()
 * @method BeeType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BeeTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BeeType::class);
    }

    // /**
    //  * @return BeeType[] Returns an array of BeeType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BeeType
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
