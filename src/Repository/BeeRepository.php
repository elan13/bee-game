<?php

namespace App\Repository;

use App\Entity\Bee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Bee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bee[]    findAll()
 * @method Bee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BeeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Bee::class);
    }

     /**
      * @return Bee[] Returns an array of Bee objects
      */
    public function findBySession($sessionId)
    {
        return $this->createQueryBuilder('b')
            ->join('b.type', 't')
            ->addSelect('t')
            ->andWhere('b.sessionId = :sessionId')
            ->setParameter('sessionId', $sessionId)
            ->getQuery()
            ->getResult()
        ;
    }
}
